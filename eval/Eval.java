package eval;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Eval {

	protected double objective;
	protected int dim;
	protected List<Double> min;
	protected List<Double> max;
	protected double errorDelta;

	protected AtomicInteger count;

	public abstract double evaluate(List<Double> value);

	public Double getMin(int i) {
		return min.get(i);
	}

	public Double getMax(int i) {
		return max.get(i);
	}

	public double getErrorDelta() {
		return errorDelta;
	}

	public double getObjective() {
		return objective;
	}

	public int getDim() {
		return dim;
	}

	public int getCount() {
		return count.intValue();
	}
}
