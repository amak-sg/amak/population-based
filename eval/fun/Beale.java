package eval.fun;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import eval.Eval;

public class Beale extends Eval {

	public Beale() {
		count = new AtomicInteger(0);

		dim = 2;
		objective = (double) 0;
		errorDelta = 0.000_01;

		min = new ArrayList<Double>();
		max = new ArrayList<Double>();
		for (int i = 0; i < dim; i++) {
			min.add((double) -4.5);
			max.add((double) 4.5);
		}
	}

	@Override
	public double evaluate(List<Double> value) {
		count.getAndIncrement();

		double x = value.get(0);
		double y = value.get(1);

		double result = Math.pow(1.5 - x * (1 - y), 2) + Math.pow(2.25 - x * (1 - y * y), 2)
				+ Math.pow(2.625 - x * (1 - y * y * y), 2);
		return result;
	}
}