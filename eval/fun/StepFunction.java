package eval.fun;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import eval.Eval;

public class StepFunction extends Eval {

	public StepFunction() {
		count = new AtomicInteger(0);

		dim = 30;
		objective = (double) 0;
		errorDelta = 0.01;

		min = new ArrayList<Double>();
		max = new ArrayList<Double>();
		for (int i = 0; i < dim; i++) {
			min.add((double) -100);
			max.add((double) 100);
		}
	}

	@Override
	public double evaluate(List<Double> value) {
		count.getAndIncrement();
		double result = 0;

		for (int i_dim = 0; i_dim < dim; i_dim++) {
			result += Math.pow(Math.floor(0.5 + value.get(i_dim)), 2);
		}
		return result;
	}

}
