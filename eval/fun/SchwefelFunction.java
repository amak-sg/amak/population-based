package eval.fun;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import eval.Eval;

public class SchwefelFunction extends Eval {

	public SchwefelFunction() {
		count = new AtomicInteger(0);

		dim = 30;
		objective = (double) -418.9829 * dim;
		errorDelta = 0.001;

		min = new ArrayList<Double>();
		max = new ArrayList<Double>();
		for (int i = 0; i < dim; i++) {
			min.add((double) -500);
			max.add((double) 500);
		}
	}

	@Override
	public double evaluate(List<Double> value) {
		count.getAndIncrement();
		double result = 0;

		for (int i_dim = 0; i_dim < dim; i_dim++) {
			result += (value.get(i_dim) * Math.sin(Math.sqrt(Math.abs(value.get(i_dim)))));
		}

		return result;
	}

}
