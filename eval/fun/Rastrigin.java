package eval.fun;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import eval.Eval;

public class Rastrigin extends Eval {

	public Rastrigin() {
		count = new AtomicInteger(0);

		dim = 30;
		objective = (double) 0;
		errorDelta = 0.001;

		min = new ArrayList<Double>();
		max = new ArrayList<Double>();
		for (int i = 0; i < dim; i++) {
			min.add((double) -5.12);
			max.add((double) 5.12);
		}
	}

	@Override
	public double evaluate(List<Double> value) {
		count.getAndIncrement();
		double result = 10 * dim;

		for (int i_dim = 0; i_dim < dim; i_dim++) {
			result += (value.get(i_dim) * value.get(i_dim) - 10 * Math.cos(2 * Math.PI * value.get(i_dim)));
		}

		return result;
	}

}