package eval.fun;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import eval.Eval;

public class SchwefelFunction1_2 extends Eval {

	public SchwefelFunction1_2() {
		count = new AtomicInteger(0);

		dim = 30;
		objective = (double) 0;
		errorDelta = 0.001;

		min = new ArrayList<Double>();
		max = new ArrayList<Double>();
		for (int i = 0; i < dim; i++) {
			min.add((double) -100);
			max.add((double) 100);
		}
	}

	@Override
	public double evaluate(List<Double> value) {
		count.getAndIncrement();
		double result = 0;

		for (int i_dim = 0; i_dim < dim; i_dim++) {
			double tmp_res = 0;
			for (int j_dim = 0; j_dim < i_dim; j_dim++) {
				tmp_res += value.get(j_dim);
			}
			result += tmp_res * tmp_res;
		}

		return result;
	}

}
