package bacteria;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import baseOptiAgent.Env;

public class BacEnv extends Env {

	List<Bacteria> agents;

	public void initAgent(List<Bacteria> _agents) {
		agents = new ArrayList<Bacteria>(_agents);
	}

	public void sortAgents() {
		Collections.sort(agents, new Comparator<Bacteria>() {
			public int compare(Bacteria s1, Bacteria s2) {
				return ((Double) s1.getHealth()).compareTo(((Double) s2.getHealth()));
			}
		});
	}

	public List<Double> getNewGenValue(int id) {
		if (id < (agents.size() / 2)) {
			return agents.get(id).getVector();
		}
		return agents.get(id / 2).getVector();
	}

	public double getNewGenFit(int id) {
		if (id < (agents.size() / 2)) {
			return agents.get(id).getFitness();
		}
		return agents.get(id / 2).getFitness();
	}

	public double getNewGenEval(int id) {
		if (id < (agents.size() / 2)) {
			return agents.get(id).getEvaluate();
		}
		return agents.get(id / 2).getEvaluate();
	}

	public List<List<Double>> getValues() {
		List<List<Double>> res = new ArrayList<List<Double>>();
		for (Bacteria agent : agents) {
			res.add(agent.getVector());
		}
		return res;
	}

	@Override
	public double getBestFitness() {
		double res = agents.get(0).getFitness();
		for (Bacteria agent : agents) {
			if (res < agent.getFitness()) {
				res = agent.getFitness();
			}
		}
		return res;
	}

	@Override
	public double getBestEval() {
		Bacteria res = agents.get(0);
		for (Bacteria agent : agents) {
			if (res.getFitness() < agent.getFitness()) {
				res = agent;
			}
		}
		return res.getEvaluate();
	}
}
