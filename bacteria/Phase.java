package bacteria;

public enum Phase {
	CHEMOTAXIS, ENV, REPRODUCTION, ELIMINATION
}
