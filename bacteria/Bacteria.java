package bacteria;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.BaseAgent;
import bee.BeeEnv;
import eval.Eval;

public class Bacteria extends BaseAgent {

	private BacEnv env;

	private List<Double> del;

	private double health;

	private Phase phase;

	private int repCount;

	// [PARAM]
	private int maxChemotaxis;
	private int maxSwim;
	private double stepSize;

	private double dAttractant;
	private double wAttractant;
	private double hRepellant;
	private double wRepellant;

	private int maxRepr;

	private double ped; // [0,1] -> probability elimination

	// SWARFMING
	List<List<Double>> bacteriaValues;

	// REPRODUCTION
	List<Double> nextGenValue;
	double nextGenFit;
	double nectGenEval;

	public Bacteria(int _id, Eval _eval, BacEnv _env, int _maxChemotaxis, int _maxSwim, double _stepSize,
			double _dAttractant, double _wAttractant, double _hRepellant, double _wRepellant, double _ped,
			int _maxRepr) {
		super(_eval, _id);
		env = _env;

		maxChemotaxis = _maxChemotaxis;
		maxSwim = _maxSwim;
		stepSize = _stepSize;

		dAttractant = _dAttractant;
		wAttractant = _wAttractant;
		hRepellant = _hRepellant;
		wRepellant = _wRepellant;
		ped = _ped;
		maxRepr = _maxRepr;

		phase = Phase.CHEMOTAXIS;

		repCount = 0;

		vector = generateRandomVector();
		evaluate = this.evaluate(vector);
		fitness = this.fitness(evaluate);
	}

	private void generateDel() {
		del = new ArrayList<Double>();
		double norm = 0;

		for (int i = 0; i < eval.getDim(); i++) {
			del.add((Math.random() - 0.5) * 2);
			norm += (del.get(i) * del.get(i));
		}
		norm = Math.sqrt(norm);
		for (int i = 0; i < eval.getDim(); i++) {
			del.set(i, del.get(i) / norm);
		}
	}

	private double computeJcc() {
		double jcc = 0;

		for (int i = 0; i < bacteriaValues.size(); i++) {

			double tmp = 0;
			for (int m = 0; m < eval.getDim(); m++) {
				tmp += Math.pow(vector.get(m) - bacteriaValues.get(i).get(m), 2);
			}
			jcc += (-dAttractant * Math.exp(-wAttractant * tmp));
		}

		for (int i = 0; i < bacteriaValues.size(); i++) {

			double tmp = 0;
			for (int m = 0; m < eval.getDim(); m++) {
				tmp += Math.pow(vector.get(m) - bacteriaValues.get(i).get(m), 2);
			}
			jcc += (-hRepellant * Math.exp(-wRepellant * tmp));
		}

		return jcc;
	}

	private void chemotaxis() {

		int chemo = 0;

		health = 0;

		while (chemo < maxChemotaxis) {

			generateDel();

			double jcc = computeJcc();
			double jlast = fitness + jcc;

			int swim = 0;

			while (chemo < maxChemotaxis && swim < maxSwim && fitness + jcc <= jlast) {
				swim++;
				jlast = fitness + jcc;

				List<Double> newVector = new ArrayList<Double>();

				for (int i = 0; i < eval.getDim(); i++) {
					newVector.add(vector.get(i) + stepSize * del.get(i));
				}

				boundValue(newVector);
				compareAndUpdate(newVector);
				jcc = computeJcc();
				health = getHealth() + fitness + jcc;

			}
			chemo++;
		}
	}

	private void reproduction() {
		vector = nextGenValue;
		evaluate = nectGenEval;
		fitness = nextGenFit;
		repCount++;
	}

	private void elimination() {

		if (repCount >= maxRepr) {
			return;
		}
		repCount = 0;

		// Elimination
		if (Math.random() > ped) {
			// PASS
		} else {
			vector = generateRandomVector();
			evaluate = this.evaluate(vector);
			fitness = this.fitness(evaluate);
		}
	}

	private void nextPhase() {
		if (phase == Phase.CHEMOTAXIS) {
			phase = Phase.ENV;
		} else if (phase == Phase.ENV) {
			phase = Phase.REPRODUCTION;
		} else if (phase == Phase.REPRODUCTION) {
			phase = Phase.ELIMINATION;
		} else {
			phase = Phase.CHEMOTAXIS;
		}
	}

	@Override
	public void perceive() {
		if (phase == Phase.CHEMOTAXIS) {
			bacteriaValues = env.getValues();
		} else if (phase == Phase.ENV) {

		} else if (phase == Phase.REPRODUCTION) {
			nextGenValue = env.getNewGenValue(id);
			nextGenFit = env.getNewGenFit(id);
			nectGenEval = env.getNewGenEval(id);
		} else {

		}
	}

	@Override
	public void act() {
		if (phase == Phase.CHEMOTAXIS) {
			chemotaxis();
		} else if (phase == Phase.ENV) {
			if (id == 0) {
				env.sortAgents();
			}
		} else if (phase == Phase.REPRODUCTION) {
			reproduction();
		} else {
			elimination();
		}
		nextPhase();
	}

	public double getHealth() {
		return health;
	}

	@Override
	public String toString() {
		return "[" + id + "] Eval / Fit -> " + this.evaluate + " / " + this.fitness + "\n" + "Vec -> " + this.vector;
	}

}
