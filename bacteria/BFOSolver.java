package bacteria;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.Result;
import baseOptiAgent.Solver;
import eval.Eval;

public class BFOSolver extends Solver {

	public BFOSolver(Eval _eval, int _maxCycle, int _maxEval) {
		super(_eval, _maxCycle, _maxEval);
		name = "BFO";
	}

	@Override
	public Result solve() {

		// [PARAM]
		int nbrAgent = 50;

		int maxChemotaxis = 100;
		int maxSwim = 4;
		double stepSize = 0.1;
		double dAttractant = 0.1;
		double wAttractant = 0.2;
		double hRepellant = 0.1;
		double wRepellan = 10;
		double ped = 0.25;
		int maxRepr = 4;

		// [INIT]
		BacEnv env = new BacEnv();

		List<Bacteria> agents = new ArrayList<Bacteria>();
		for (int i_agent = 0; i_agent < nbrAgent; i_agent++) {
			agents.add(new Bacteria(i_agent, eval, env, maxChemotaxis, maxSwim, stepSize, dAttractant, wAttractant,
					hRepellant, wRepellan, ped, maxRepr));
		}

		env.initAgent(agents);

		return findSolution(agents, env, 4);
	}

}
