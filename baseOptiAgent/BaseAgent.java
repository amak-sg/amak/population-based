package baseOptiAgent;

import java.util.ArrayList;
import java.util.List;

import eval.Eval;
import mas.core.Agent;

public abstract class BaseAgent extends Agent {

	protected List<Double> vector;
	protected double fitness;
	protected double evaluate;

	protected Eval eval;

	protected int id;

	public BaseAgent(Eval _eval, int _id) {
		eval = _eval;
		id = _id;
	}

	protected void boundValue(List<Double> vector) {
		for (int i = 0; i < vector.size(); i++) {
			if (vector.get(i) > eval.getMax(i)) {
				vector.set(i, eval.getMax(i));
			} else if (vector.get(i) < eval.getMin(i)) {
				vector.set(i, eval.getMin(i));
			}
		}
	}

	protected List<Double> generateRandomVector() {
		List<Double> result = new ArrayList<Double>();

		for (int i = 0; i < eval.getDim(); i++) {
			result.add(eval.getMin(i) + Math.random() * (eval.getMax(i) - eval.getMin(i)));
		}
		boundValue(result);
		return result;
	}

	protected double evaluate(List<Double> vector) {
		return eval.evaluate(vector);
	}

	protected double fitness(double value) {
		if (value >= 0) {
			return 1 / (1 + value);
		} else {
			return 1 + Math.abs(value);
		}
	}

	public int getId() {
		return id;
	}

	public List<Double> getVector() {
		return vector;
	}

	public double getEvaluate() {
		return evaluate;
	}

	public double getFitness() {
		return fitness;
	}

	protected boolean compareAndUpdate(List<Double> otherVec) {
		double newEval = evaluate(otherVec);
		double newFitness = fitness(newEval);
		if (fitness < newFitness) {
			vector = otherVec;
			fitness = newFitness;
			evaluate = newEval;
			return true;
		}
		return false;
	}

}
