package baseOptiAgent;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import aabc.ABeeSolver;
import amakaque.SMSolver;
import ant.AntSolver;
import bacteria.BFOSolver;
import bee.BeeSolver;
import eval.Eval;
import eval.fun.AxisParallelHyper_Ellipsoid;
import eval.fun.Beale;
import eval.fun.Cigar;
import eval.fun.Rastrigin;
import eval.fun.SchwefelFunction1_2;
import firefly.FASolver;
import smac.SmacSolver;

public class Ihm {
	public static void main(String[] args) {

		// [PARAM]

		// Eval eval = new StepFunction();
		// Eval eval = new SchwefelFunction1_2();
		// Eval eval = new SchwefelFunction();
		Eval eval = new Rastrigin();
		// Eval eval = new Cigar();
		// Eval eval = new AxisParallelHyper_Ellipsoid();
		// Eval eval = new Beale();

		int maxCycle = 1_000_000;
		int maxEval = 1_000_000;

		/*
		 * // [INIT] //Solver solver = new SMSolver(eval, maxCycle, maxEval); //Solver
		 * solver = new BeeSolver(eval, maxCycle, maxEval); //Solver solver = new
		 * AntSolver(eval, maxCycle, maxEval); //Solver solver = new ABeeSolver(eval,
		 * maxCycle, maxEval); Solver solver = new FASolver(eval, maxCycle, maxEval);
		 * 
		 * // [SOLVING] Result res = solver.solve();
		 * 
		 * 
		 * // [RESULT]
		 * 
		 * System.out.println("Cycle : "+res.totalCycle+" Out of "+maxCycle);
		 * System.out.println("Eval : "+res.totalEval+" Out of "+maxEval);
		 * System.out.println("Solution found "+res.optiFound);
		 * 
		 * writeEval(res);
		 */

		startAll(maxCycle, maxEval);
	}

	private static void writeEval(Result res) {
		try {
			FileWriter csvWriter = new FileWriter("eval_" + res.name + ".csv");
			csvWriter.append("cycle,eval,sol\n");

			for (Integer name : res.cycle.keySet()) {
				csvWriter.append(name + "," + res.cycle.get(name).getEval() + ","
						+ res.cycle.get(name).getBestSolution() + "\n");
			}
			csvWriter.flush();
			csvWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void startAll(int maxCycle, int maxEval) {
		Eval eval = new SchwefelFunction1_2();
		Solver solver = new SMSolver(new SchwefelFunction1_2(), maxCycle, maxEval);
		writeEval(solver.solve());

		solver = new BeeSolver(new SchwefelFunction1_2(), maxCycle, maxEval);
		writeEval(solver.solve());

		solver = new AntSolver(new SchwefelFunction1_2(), maxCycle, maxEval);
		writeEval(solver.solve());

		solver = new ABeeSolver(new SchwefelFunction1_2(), maxCycle, maxEval);
		writeEval(solver.solve());

		solver = new FASolver(new SchwefelFunction1_2(), maxCycle, maxEval);
		writeEval(solver.solve());

		solver = new BFOSolver(new SchwefelFunction1_2(), maxCycle, maxEval);
		writeEval(solver.solve());

		System.out.println("Done");
	}

	private void benchmarkAlgo(int maxCycle, int maxEval) {

		int nbrTry = 100;

		for (int i = 0; i < nbrTry; i++) {
			Eval eval = new Rastrigin();
			Solver solver = new SMSolver(eval, maxCycle, maxEval);
			Result res = solver.solve();
		}
	}
}
