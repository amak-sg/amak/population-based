package baseOptiAgent;

import eval.Eval;

public abstract class Env {

	protected Eval eval;

	public abstract double getBestFitness();

	public abstract double getBestEval();
}
