package baseOptiAgent;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Result {

	public String name;

	public int totalEval;
	public int totalCycle;

	public boolean optiFound;

	public Map<Integer, Cycle> cycle;

	public Result(String _name) {
		name = _name;
		optiFound = false;
		cycle = new TreeMap<Integer, Cycle>();
	}

}
