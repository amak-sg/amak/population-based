package ant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import baseOptiAgent.Env;
import eval.Eval;

public class AntEnv extends Env {

	private List<Solution> archive;
	private List<Double> archiveProba;

	private Eval eval;

	private List<Ant> agents;

	private int archiveLen;
	private double q;

	public AntEnv(int _archiveLen, double _q, Eval _eval) {
		archiveLen = _archiveLen;
		q = _q;

		archive = new ArrayList<Solution>();
		archiveProba = new ArrayList<Double>();

		eval = _eval;
	}

	public void initAgents(List<Ant> _agents) {
		agents = new ArrayList<Ant>(_agents);
		updateArchive();
	}

	private void sortArchive() {
		Collections.sort(archive, new Comparator<Solution>() {
			@Override
			public int compare(Solution o1, Solution o2) {
				if (o1.getEval() > o2.getEval()) {
					return 1;
				}
				if (o1.getEval() < o2.getEval()) {
					return -1;
				}
				return 0;
			}
		});
	}

	private void sortAndRemoveSolution() {
		sortArchive();

		while (archive.size() > archiveLen) {
			archive.remove(archive.size() - 1);
		}
	}

	public void updateArchive() {
		for (Ant agent : agents) {
			archive.add(new Solution(agent.getVector(), agent.getEvaluate()));
		}

		sortAndRemoveSolution();
	}

	public void computeProba() {
		archiveProba = new ArrayList<Double>();

		List<Double> w = new ArrayList<Double>();
		double wMax = 0;

		for (int i = 0; i < archiveLen; i++) {
			double wi = 1 / (q * archiveLen * Math.sqrt(2 * Math.PI))
					* Math.exp(-Math.pow(i, 2) / (2 * Math.pow(q, 2) * Math.pow(archiveLen, 2)));
			w.add(wi);
			wMax += wi;
		}

		for (int i = 0; i < archiveLen; i++) {
			archiveProba.add(w.get(i) / wMax);
		}
	}

	public List<Solution> getArchive() {
		return archive;
	}

	public List<Double> getArchiveProba() {
		return archiveProba;
	}

	public Eval getEval() {
		return eval;
	}

	public List<Ant> getAgents() {
		return agents;
	}

	@Override
	public double getBestFitness() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getBestEval() {
		return archive.get(archive.size() - 1).getEval();
	}
}
