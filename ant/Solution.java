package ant;

import java.util.ArrayList;
import java.util.List;

public class Solution {
	private List<Double> vector;
	private double eval;

	public Solution(List<Double> _vector, double _eval) {
		eval = _eval;
		vector = new ArrayList<Double>(_vector);
	}

	public double getEval() {
		return eval;
	}

	public List<Double> getVector() {
		return vector;
	}

	@Override
	public String toString() {
		return "" + eval;
	}
}
