package ant;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.Result;
import baseOptiAgent.Solver;
import eval.Eval;

public class AntSolver extends Solver {

	public AntSolver(Eval _eval, int _maxCycle, int _maxEval) {
		super(_eval, _maxCycle, _maxEval);
		name = "Ant";
	}

	@Override
	public Result solve() {

		// [PARAM]
		int nbrAgent = 40;
		int archiveSize = 10;

		double evaporation = 0.6;
		double q = 0.2;

		// [INIT]
		AntEnv env = new AntEnv(archiveSize, q, eval);

		List<Ant> agents = new ArrayList<Ant>();
		for (int i_agent = 0; i_agent < nbrAgent; i_agent++) {
			agents.add(new Ant(i_agent, eval, env, evaporation));
		}

		env.initAgents(agents);

		return findSolution(agents, env, 2);
	}

}