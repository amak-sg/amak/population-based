package ant;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import baseOptiAgent.BaseAgent;
import eval.Eval;

public class Ant extends BaseAgent {

	AntEnv env;

	Phase phase;

	private double evaporationRate;

	// EVAL

	// BUILD NEW
	private List<Solution> archive;
	private List<Double> archiveProba;

	public Ant(int _id, Eval _eval, AntEnv _env, double _evaporationRate) {
		super(_eval, _id);

		env = _env;

		evaporationRate = _evaporationRate;

		vector = new ArrayList<Double>();
		for (int i = 0; i < eval.getDim(); i++) {
			vector.add(eval.getMin(i) + Math.random() * (eval.getMax(i) - eval.getMin(i)));
		}

		evaluate = evaluate(vector);

		phase = Phase.EVALUATE;
	}

	private void nextPhase() {
		if (phase == Phase.EVALUATE) {
			phase = Phase.BUILD_NEW;
		} else if (phase == Phase.BUILD_NEW) {
			phase = Phase.EVALUATE;
		}
	}

	@Override
	public void perceive() {
		/*
		 * if(id == 0) { System.out.println("\n\n"); } System.out.println(this);
		 */

		if (phase == Phase.EVALUATE) {
			if (id == 0) {

			}
		} else if (phase == Phase.BUILD_NEW) {
			archive = env.getArchive();
			archiveProba = env.getArchiveProba();
		}
	}

	@Override
	public void act() {
		if (phase == Phase.EVALUATE) {
			if (id == 0) {
				env.updateArchive();
				env.computeProba();
			}
		} else if (phase == Phase.BUILD_NEW) {

			vector = buildNewSolution();
			evaluate = evaluate(vector);

		}
		nextPhase();
	}

	public int chooseSolution() {
		double prob = Math.random();

		double res = archiveProba.get(0);
		int i = 0;
		while (res < prob) {
			i++;
			res += archiveProba.get(i);
		}
		return i;
	}

	public List<Double> buildNewSolution() {

		int archiveLen = archive.size();

		Random ran = new Random();

		List<Double> sol = new ArrayList<Double>();

		for (int j = 0; j < eval.getDim(); j++) {
			int i_archive = chooseSolution();
			List<Double> init = archive.get(i_archive).getVector();

			double d = 0;
			for (int i = 0; i < archiveLen; i++) {
				d += Math.abs(archive.get(i).getVector().get(j) - init.get(j)) / (archiveLen - 1);
			}

			double o = evaporationRate * d;

			double possibleSol = ran.nextGaussian(init.get(j), o);

			if (possibleSol > eval.getMax(j)) {
				possibleSol = eval.getMax(j);
			} else if (possibleSol < eval.getMin(j)) {
				possibleSol = eval.getMin(j);
			}

			sol.add(possibleSol);
		}

		return sol;
	}

	public Phase getPhase() {
		return phase;
	}

	@Override
	public String toString() {
		return "[AGENT] " + id + "\n" + " - Phase " + phase + "\n" + " - Eval " + evaluate + "\n" + " - Vector" + vector
				+ "\n";
	}
}
