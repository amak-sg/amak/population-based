package firefly;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.Result;
import baseOptiAgent.Solver;
import eval.Eval;

public class FASolver extends Solver {

	public FASolver(Eval _eval, int _maxCycle, int _maxEval) {
		super(_eval, _maxCycle, _maxEval);
		name = "Firefly";
	}

	@Override
	public Result solve() {

		// [PARAM]
		int nbrAgent = 10;
		double bMax = 0.2; // [0, 1]
		double y = 1; // [0.1, 10] théoriquement [0, inf)
		double randomAlpha = 0.5; // [0, 1]

		// [INIT]
		FAEnv env = new FAEnv(bMax, y);

		List<Firefly> agents = new ArrayList<Firefly>();
		for (int i_agent = 0; i_agent < nbrAgent; i_agent++) {
			agents.add(new Firefly(i_agent, eval, env, randomAlpha));
		}

		env.initAgent(agents);

		Result res = findSolution(agents, env, 2);

		return res;
	}

}
