package firefly;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.Env;

public class FAEnv extends Env {

	private List<Firefly> agents;
	private List<List<Double>> attractiveness;

	private double bMax;
	private double y;

	public FAEnv(double _bMax, double _y) {
		bMax = _bMax;
		y = _y;
	}

	public void initAgent(List<Firefly> _agents) {
		agents = new ArrayList<Firefly>(_agents);
	}

	private double distance(List<Double> a, List<Double> b) {
		double res = 0;
		for (int i = 0; i < a.size(); i++) {
			res += Math.pow(a.get(i) - b.get(i), 2);
		}
		return Math.sqrt(res);
	}

	public void computeAttractiveness() {
		attractiveness = new ArrayList<List<Double>>();

		for (int i = 0; i < agents.size(); i++) {
			List<Double> tmp = new ArrayList<Double>();
			for (int j = 0; j < i; j++) {
				double dist = distance(agents.get(i).getVector(), agents.get(j).getVector());
				tmp.add(bMax * Math.exp(-y * dist));
			}
			attractiveness.add(tmp);
		}

	}

	public List<List<Double>> getAttractiveness() {
		return attractiveness;
	}

	public List<Double> getFitness() {
		List<Double> res = new ArrayList<Double>();
		for (int i = 0; i < agents.size(); i++) {
			res.add(agents.get(i).getFitness());
		}
		return res;
	}

	public List<List<Double>> getValues() {
		List<List<Double>> res = new ArrayList<List<Double>>();
		for (int i = 0; i < agents.size(); i++) {
			res.add(agents.get(i).getVector());
		}
		return res;

	}

	@Override
	public double getBestFitness() {
		double res = agents.get(0).getFitness();
		for (Firefly agent : agents) {
			if (res < agent.getFitness()) {
				res = agent.getFitness();
			}
		}
		return res;
	}

	@Override
	public double getBestEval() {
		Firefly res = agents.get(0);
		for (Firefly agent : agents) {
			if (res.getFitness() < agent.getFitness()) {
				res = agent;
			}
		}
		return res.getEvaluate();
	}
}
