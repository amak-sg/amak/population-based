package aabc;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.BaseAgent;
import eval.Eval;

public class Bee extends BaseAgent {

	BeeEnv env;

	int maxCount;

	Phase phase;
	int stuckCount;

	// EMPLOYED
	List<List<Double>> randomAgents;
	double groupValue;

	// ONLOOKERS
	double bestFitness;
	// List<List<Double>> randomAgents;
	// double groupValue;

	public Bee(Eval _eval, int _id, BeeEnv _env, int _maxCount) {
		super(_eval, _id);

		env = _env;

		maxCount = _maxCount;

		phase = Phase.EMPLOYED;
		stuckCount = 0;

		vector = generateRandomVector();
		evaluate = this.evaluate(vector);
		fitness = this.fitness(evaluate);
	}

	private void nextPhase() {
		if (phase == Phase.EMPLOYED) {
			phase = Phase.ONLOOKERS;
		} else if (phase == Phase.ONLOOKERS) {
			phase = Phase.SCOUTS;
		} else if (phase == Phase.SCOUTS) {
			phase = Phase.EMPLOYED;
		}
	}

	@Override
	public void perceive() {

		if (phase == Phase.EMPLOYED) {
			randomAgents = env.getRandomAgents(id);
			groupValue = env.getGroup(this.getFitness());
		} else if (phase == Phase.ONLOOKERS) {
			randomAgents = env.getRandomAgents(id);
			groupValue = env.getGroup(this.getFitness());
			bestFitness = env.getBestFitness();
		} else if (phase == Phase.SCOUTS) {
		}

	}

	private boolean randomlyUpdate() {

		List<Double> newVector = new ArrayList<Double>(vector);

		for (int i_dim = 0; i_dim < eval.getDim(); i_dim++) {
			if (Math.random() < groupValue) {
				newVector.set(i_dim, vector.get(i_dim)
						+ 2 * (Math.random() - 0.5) * (vector.get(i_dim) - randomAgents.get(i_dim).get(i_dim)));
			}
		}

		boundValue(newVector);

		return compareAndUpdate(newVector);
	}

	private void employedPhase() {
		boolean res = randomlyUpdate();
		if (!res) {
			stuckCount++;
		} else {
			stuckCount = 0;
		}
	}

	private void onlookerPhase() {

		// change p to this ? https://www.hindawi.com/journals/jam/2014/402616/
		double p = (0.9 * fitness / bestFitness) + 0.1;

		if (Math.random() > p) {
			randomlyUpdate();
		}
	}

	private void scoutPhase() {
		if (stuckCount >= maxCount) {
			stuckCount = 0;

			double oldEval = evaluate;
			vector = generateRandomVector();
			evaluate = this.evaluate(vector);
			fitness = this.fitness(evaluate);
		}
	}

	@Override
	public void act() {

		if (phase == Phase.EMPLOYED) {
			employedPhase();
		} else if (phase == Phase.ONLOOKERS) {
			onlookerPhase();
		} else if (phase == Phase.SCOUTS) {
			scoutPhase();
		}
		nextPhase();
	}

	public Phase getPhase() {
		return phase;
	}

}
