package aabc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import baseOptiAgent.Env;
import eval.Eval;

public class BeeEnv extends Env {

	private List<Bee> agents;

	public BeeEnv(Eval _eval) {
		eval = _eval;
	}

	public void initAgent(List<Bee> _agents) {
		agents = new ArrayList<Bee>(_agents);
	}

	public List<List<Double>> getRandomAgents(int id) {
		if (agents.size() == 1) {
			throw new java.lang.Error("Cannot return random agent because the environment only know 1 agent.");
		}

		Random rand = new Random();

		List<List<Double>> randomAgents = new ArrayList<List<Double>>();

		Bee randomagent = agents.get(rand.nextInt(agents.size()));
		while (randomagent.getId() == id) {
			randomagent = agents.get(rand.nextInt(agents.size()));
		}
		for (int i = 0; i < eval.getDim(); i++) {
			randomAgents.add(randomagent.getVector());
		}

		/*
		 * for (int i = 0; i < eval.getDim(); i++) { Bee randomagent =
		 * agents.get(rand.nextInt(agents.size())); while (randomagent.getId() == id) {
		 * randomagent = agents.get(rand.nextInt(agents.size())); }
		 * randomAgents.add(randomagent.getVector()); }
		 */

		return randomAgents;
	}

	public double getGroup(double fitness) {
		int res = 0;
		for (Bee bee : agents) {
			if (bee.getFitness() > fitness) {
				res++;
			}
		}
		return Math.ceil((double) res * 5.0 / (double) agents.size()) / 10;
	}

	@Override
	public double getBestFitness() {
		double res = agents.get(0).getFitness();
		for (Bee agent : agents) {
			if (res < agent.getFitness()) {
				res = agent.getFitness();
			}
		}
		return res;
	}

	@Override
	public double getBestEval() {
		Bee res = agents.get(0);
		for (Bee agent : agents) {
			if (res.getFitness() < agent.getFitness()) {
				res = agent;
			}
		}
		return res.getEvaluate();
	}

}
