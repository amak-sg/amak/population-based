package aabc;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.BaseAgent;
import baseOptiAgent.Env;
import baseOptiAgent.Result;
import baseOptiAgent.Solver;
import eval.Eval;

public class ABeeSolver extends Solver {

	public ABeeSolver(Eval _eval, int _maxCycle, int _maxEval) {
		super(_eval, _maxCycle, _maxEval);
		name = "A-Bee";
	}

	@Override
	public Result solve() {

		// [PARAM]
		int nbrAgent = 100;
		int maxTry = 200;

		// [INIT]
		BeeEnv env = new BeeEnv(eval);

		List<Bee> agents = new ArrayList<Bee>();
		for (int i_agent = 0; i_agent < nbrAgent; i_agent++) {
			agents.add(new Bee(eval, i_agent, env, maxTry));
		}

		env.initAgent(agents);

		return findSolution(agents, env, 3);
	}

}
