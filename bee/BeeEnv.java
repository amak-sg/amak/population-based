package bee;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import baseOptiAgent.Env;

public class BeeEnv extends Env {

	private List<Bee> agents;

	public void initAgent(List<Bee> _agents) {
		agents = new ArrayList<Bee>(_agents);
	}

	public List<Double> getRandomAgent(int id) {
		if (agents.size() == 1) {
			throw new java.lang.Error("Cannot return random agent because the environment only know 1 agent.");
		}

		Random rand = new Random();
		Bee randomagent = agents.get(rand.nextInt(agents.size()));
		while (randomagent.getId() == id) {
			randomagent = agents.get(rand.nextInt(agents.size()));
		}
		return randomagent.getVector();
	}

	@Override
	public double getBestFitness() {
		double res = agents.get(0).getFitness();
		for (Bee agent : agents) {
			if (res < agent.getFitness()) {
				res = agent.getFitness();
			}
		}
		return res;
	}

	@Override
	public double getBestEval() {
		Bee res = agents.get(0);
		for (Bee agent : agents) {
			if (res.getFitness() < agent.getFitness()) {
				res = agent;
			}
		}
		return res.getEvaluate();
	}

}
