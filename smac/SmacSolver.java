package smac;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.Result;
import baseOptiAgent.Solver;
import eval.Eval;

public class SmacSolver extends Solver {

	public SmacSolver(Eval _eval, int _maxCycle, int _maxEval) {
		super(_eval, _maxCycle, _maxEval);
		name = "Smac";
	}

	@Override
	public Result solve() {

		int nAgents = 50;
		double perception = 50.0;
		double maxDensity = 0.3;
		int maxInactivity = 5;
		int maxMemory = 1500;

		List<Smagent> agents = new ArrayList<Smagent>();

		SmacEnv env = new SmacEnv(perception);

		for (int i = 0; i < nAgents; i++) {
			agents.add(new Smagent(eval, i, env, nAgents, maxDensity, maxInactivity, maxMemory));
		}

		env.initAgents(agents);

		return findSolution(agents, env, 2);
	}

}
