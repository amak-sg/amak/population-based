package smac;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.Env;

public class SmacEnv extends Env {

	private List<Smagent> agents;
	private List<List<Double>> distance;
	private List<Double> density;

	private double maxDist;

	public SmacEnv(double _maxDist) {
		maxDist = _maxDist;
	}

	public void initAgents(List<Smagent> _agents) {
		agents = _agents;
	}

	private double dist(List<Double> a, List<Double> b) {
		double res = 0;
		for (int i = 0; i < a.size(); i++) {
			res += Math.pow(a.get(i) - b.get(i), 2);
		}
		return Math.sqrt(res);
	}

	public void computeDistance() {
		distance = new ArrayList<List<Double>>();
		for (int i_agent = 0; i_agent < agents.size(); i_agent++) {
			List<Double> tmpDist = new ArrayList<Double>();
			for (int j_agent = 0; j_agent < agents.size(); j_agent++) {
				tmpDist.add(0.0);
			}
			distance.add(tmpDist);
		}

		for (int i_agent = 0; i_agent < agents.size(); i_agent++) {
			for (int j_agent = 0; j_agent < i_agent; j_agent++) {
				double dist = dist(agents.get(i_agent).getVector(), agents.get(j_agent).getVector());
				distance.get(i_agent).set(j_agent, dist);
				distance.get(j_agent).set(i_agent, dist);
			}
		}
	}

	public List<List<Double>> getNei(int id) {
		List<List<Double>> res = new ArrayList<List<Double>>();
		for (int i_agent = 0; i_agent < agents.size(); i_agent++) {
			if (distance.get(id).get(i_agent) <= maxDist) {
				res.add(agents.get(i_agent).getVector());
			}
		}
		return res;
	}

	public List<Double> getFit(int id) {
		List<Double> res = new ArrayList<Double>();
		for (int i_agent = 0; i_agent < agents.size(); i_agent++) {
			if (distance.get(id).get(i_agent) <= maxDist) {
				res.add(agents.get(i_agent).getFitness());
			}
		}
		return res;
	}

	public List<Double> getDensity(int id) {
		List<Double> res = new ArrayList<Double>();
		for (int i_agent = 0; i_agent < agents.size(); i_agent++) {
			if (distance.get(id).get(i_agent) <= maxDist) {
				int nbrNei = getNei(i_agent).size();
				res.add((double) nbrNei / this.agents.size());
			}
		}
		return res;
	}

	public List<List<Double>> getMemory(int id) {
		List<List<Double>> res = new ArrayList<List<Double>>();
		for (int i_agent = 0; i_agent < agents.size(); i_agent++) {
			if (distance.get(id).get(i_agent) <= maxDist) {
				res.addAll(agents.get(i_agent).getMemoryValue());
			}
		}
		return res;
	}

	public List<Double> getMemoryFit(int id) {
		List<Double> res = new ArrayList<Double>();
		for (int i_agent = 0; i_agent < agents.size(); i_agent++) {
			if (distance.get(id).get(i_agent) <= maxDist) {
				res.addAll(agents.get(i_agent).getMemoryFit());
			}
		}
		return res;
	}

	@Override
	public double getBestFitness() {
		double res = agents.get(0).getFitness();
		for (Smagent agent : agents) {
			if (res < agent.getFitness()) {
				res = agent.getFitness();
			}
		}
		return res;
	}

	@Override
	public double getBestEval() {
		Smagent res = agents.get(0);
		for (Smagent agent : agents) {
			if (res.getFitness() < agent.getFitness()) {
				res = agent;
			}
		}
		return res.getEvaluate();
	}

	public List<Double> getBest() {
		Smagent res = agents.get(0);
		for (Smagent agent : agents) {
			if (res.getFitness() < agent.getFitness()) {
				res = agent;
			}
		}
		return res.getVector();
	}
}
