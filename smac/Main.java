package smac;

import java.util.ArrayList;
import java.util.List;

import eval.Eval;
import eval.fun.SchwefelFunction1_2;
import eval.twoDFun.Squared;
import mas.core.Agent;
import mas.core.Cyclable;
import mas.environment.TwoDContinuosGrid;
import mas.implementation.schedulers.variations.TwoDCycling;
import mas.ui.MainWindow;
import mas.ui.SchedulerToolbar;

public class Main {

	public static void main(String[] args) {

		int nAgents = 50;
		double perception = 50.0;
		double maxDensity = 0.3;
		int maxInactivity = 25;
		int maxMemory = 1500;

		List<Smagent> agents = new ArrayList<Smagent>();
		Eval eval = new Squared();
		//Eval eval = new SchwefelFunction1_2();
		
		

		SmacEnv env = new SmacEnv(perception);

		for (int i = 0; i < nAgents; i++) {
			agents.add(new Smagent(eval, i, env, nAgents, maxDensity, maxInactivity, maxMemory));
		}

		env.initAgents(agents);

		TwoDCycling scheduler = new TwoDCycling(agents.toArray(new Smagent[agents.size()]));

		MainWindow.instance();
		MainWindow.addToolbar(new SchedulerToolbar("Amas", scheduler));
	}
}
