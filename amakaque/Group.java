package amakaque;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Group {

	private AtomicInteger count;
	private int localLimitCount;

	private List<SMAgent> agents;

	public Group() {
		count = new AtomicInteger(1);
		localLimitCount = 0;
		agents = new ArrayList<SMAgent>();
	}

	public void count() {
		count.getAndIncrement();
	}

	public int getCount() {
		return count.intValue();
	}

	public void resetCount() {
		count.set(1);
	}

	public List<SMAgent> getAgents() {
		return agents;
	}

	public void removeAgent(SMAgent agent) {
		agents.remove(agent);
	}

	public void addAgents(List<SMAgent> _agents) {
		for (SMAgent agent : _agents) {
			agents.add(agent);
		}
	}

	public void addLocalLimit() {
		localLimitCount++;
	}

	public void resetLocalLimit() {
		localLimitCount = 0;
	}

	public int getLocallimit() {
		return this.localLimitCount;
	}
}
