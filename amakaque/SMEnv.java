package amakaque;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import baseOptiAgent.Env;
import eval.Eval;

public class SMEnv extends Env {

	private int globalLimitCount;
	private int localLimit;
	private int globalLimit;
	private int groupLimit;
	private List<Group> groups;

	public SMEnv(int _localLimit, int _globalLimit, int _groupLimit, Eval _eval) {
		globalLimitCount = 0;
		eval = _eval;

		localLimit = _localLimit;
		globalLimit = _globalLimit;

		groupLimit = _groupLimit;

		groups = new ArrayList<Group>();
	}

	public void initGroup(Group group) {
		groups.add(group);
	}

	public int getGlobalLimitCount() {
		return globalLimitCount;
	}

	public int getGlobalLimit() {
		return globalLimit;
	}

	public int getNbrGroup() {
		return this.groups.size();
	}

	public int getGroupLimit() {
		return this.groupLimit;
	}

	public void addGlobalLimitCount() {
		globalLimitCount++;
	}

	public void resetGlobalCount() {
		globalLimitCount = 0;
	}

	public List<Double> getRandomGroupMember(int i_group, int self_id) {
		Group group = groups.get(i_group);

		Random rand = new Random();
		SMAgent randomagent = group.getAgents().get(rand.nextInt(group.getAgents().size()));

		if (group.getAgents().size() == 1) {
			throw new java.lang.Error("The group :" + String.valueOf(i_group) + " only have 1 member.");
		}

		while (randomagent.getId() == self_id) {
			randomagent = group.getAgents().get(rand.nextInt(group.getAgents().size()));
		}

		return randomagent.getVector();
	}

	public List<Double> getLocalLeader(int i_group) {
		Group group = groups.get(i_group);

		List<SMAgent> agents = group.getAgents();
		for (SMAgent agent : agents) {
			if (agent.isLL()) {
				return agent.getVector();
			}
		}
		throw new java.lang.Error("The group :" + String.valueOf(i_group) + " don t have a local leader.");
	}

	@Override
	public double getBestFitness() {
		double max = 0;

		for (Group group : groups) {
			for (SMAgent agent : group.getAgents()) {
				double fitness = agent.getFitness();
				if (fitness > max) {
					max = fitness;
				}
			}
		}
		return max;
	}

	public List<Double> getGlobalLeader() {

		for (Group group : groups) {
			for (SMAgent agent : group.getAgents()) {
				if (agent.isGL()) {
					return agent.getVector();
				}
			}
		}
		throw new java.lang.Error("No global leader found.");
	}

	public int getGroupSize(int i_group) {
		return groups.get(i_group).getAgents().size();
	}

	public int getCount(int i_group) {
		return groups.get(i_group).getCount();
	}

	public void count(int i_group) {
		groups.get(i_group).count();
	}

	public void resetGroupCount(int i_group) {
		groups.get(i_group).resetCount();
	}

	public boolean allDone() {
		for (Group group : groups) {
			if (group.getCount() < group.getAgents().size()) {
				return false;
			}
		}
		return true;
	}

	public SMAgent findNextGL() {
		SMAgent nextGL = null;
		for (Group group : groups) {
			for (SMAgent agent : group.getAgents()) {
				if (nextGL == null) {
					nextGL = agent;
				} else if (nextGL.getFitness() < agent.getFitness()) {
					nextGL = agent;
				}
			}
		}
		return nextGL;
	}

	public void addLocalLimit(int i_group) {
		groups.get(i_group).addLocalLimit();
	}

	public SMAgent findNextLL(int i_group) {
		SMAgent nextLL = null;
		for (SMAgent agent : groups.get(i_group).getAgents()) {
			if (nextLL == null) {
				nextLL = agent;
			} else if (nextLL.getFitness() < agent.getFitness()) {
				nextLL = agent;

			}
		}
		return nextLL;
	}

	public int getLocalLimitCount(int i_group) {
		return groups.get(i_group).getLocallimit();
	}

	public void resetLocalCount(int i_group) {
		groups.get(i_group).resetLocalLimit();
	}

	public int getLocalLimit() {
		return localLimit;
	}

	public void combineAllGroups() {
		while (groups.size() > 1) {
			for (SMAgent agent : groups.get(1).getAgents()) {
				agent.giveLocalLeadership();
				agent.enterNewGroup(0);
			}

			groups.get(0).addAgents(groups.get(1).getAgents());
			groups.remove(1);
		}
	}

	public void splitInNGroups(int n) {

		for (int i = 0; i < n - 1; i++) {
			groups.add(new Group());
		}

		List<SMAgent> agents = new ArrayList<SMAgent>();
		agents.addAll(groups.get(0).getAgents());

		for (SMAgent agent : agents) {
			groups.get(0).removeAgent(agent);
		}

		for (SMAgent agent : agents) {
			agent.giveLocalLeadership();
			agent.enterNewGroup(agent.getId() % n);
			List<SMAgent> list = new ArrayList<SMAgent>();
			list.add(agent);
			groups.get(agent.getId() % n).addAgents(list);
		}
	}

	public List<Group> getGroups() {
		return groups;
	}

	public double getGLValue() {
		for (Group group : groups) {
			for (SMAgent agent : group.getAgents()) {
				if (agent.isGL()) {
					return agent.getFitness();
				}
			}
		}
		throw new java.lang.Error("No global leader found.");
	}

	@Override
	public double getBestEval() {
		SMAgent maxAgent = groups.get(0).getAgents().get(0);

		for (Group group : groups) {
			for (SMAgent agent : group.getAgents()) {
				if (agent.getFitness() > maxAgent.getFitness()) {
					maxAgent = agent;
				}
			}
		}
		return maxAgent.getEvaluate();
	}
}
